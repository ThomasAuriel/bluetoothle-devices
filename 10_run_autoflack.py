autoflake -i -r --expand-star-imports --remove-all-unused-imports --remove-unused-variables --exclude=__init__.py ./scripts/*
black -l 100 ./scripts/*