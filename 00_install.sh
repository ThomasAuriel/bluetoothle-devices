sudo apt-get install python3-dev libglib2.0-dev

python3 -m venv wheatherstation
source wheatherstation/bin/activate

pip install pip

# Bluetooth
pip install bluepy

# Grafana
sudo apt install software-properties-common
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
sudo apt update
sudo apt install grafana

sudo systemctl start grafana-server
sudo systemctl enable grafana-server

# InfluxDB
sudo apt install influxdb
pip install influxdb

sudo systemctl start influxdb
sudo systemctl enable influxdb

# dev
pip install mypy
pip install black
pip install autoflake
