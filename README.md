# Bluetooth LE Station

This repo provides a way to use a raspberry to retrieve data from Bluetooth LE devices.

The device has to have:
- A Bluetooth mac address
- Accept connections
- Have a service and UUID associated
- Have a characteristic and UUID associated
In this script, an example is provided the `Mi Temperature & Humidity Monitor 2` is used (it uses the `LYWSD03MMC` Bluetooth chip).

The raspberry used for this project is a Raspberry Pi 4. Other raspberry or computer can be used. They require the Bluetooth interface support LE.

Finally, a database is requested to store values. InfluxDB is used by this code. Other can be used if wanted.

## Raspberry - Installation

The `00_install.sh` install all the requested dependencies on a raspberry.

```sh
sudo apt-get install python3-dev libglib2.0-dev

python3 -m venv wheatherstation
source wheatherstation/bin/activate

pip install pip

# Bluetooth
pip install bluepy

# Grafana
sudo apt install software-properties-common
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
sudo apt update
sudo apt install grafana

sudo systemctl start grafana-server
sudo systemctl enable grafana-server

# InfluxDB
sudo apt install influxdb
pip install influxdb

sudo systemctl start influxdb
sudo systemctl enable influxdb
```

## LYWSD03MMC - New Firmware

Standalone LYWSD03MMC firmware is not energy efficient. Is it possible to load a more efficient one and save battery duration. With a simple test, the baterry last less than a month.

The new firmware I'm using here is [ATC_MiThermometer](https://github.com/pvvx/ATC_MiThermometer). It is possible to load easily with [this page](https://pvvx.github.io/ATC_MiThermometer/TelinkMiFlasher.html).
The new code is adapted to this firmware.

## Python code

### Adaptation of the python code

The installation request several steps.
We supposed you installed the raspberry 

#### Find the mac address of your device

Two methods are possible:

- use of Bluetoothctl
- use of a smartphone

##### Use of bluetoothctl

The commands to use the bluetoothctl are

```sh
bluetoothctl
```

Write then:

```sh
menu scan
transport le
back
scan on
```

This will ask to bluetoothctl to scan on device using LE protocol.
At this point your device shall show up. Note its Mac address for later.

##### Use of Smartphone

It is possible to use a smartphone. Many apps allow to scan for Bluetooth LE devices and show their mac address.

Once you have the mac address you have to know the Service UUID and the characteristic UUID, you can update the code. To do so you have to add it to the device list.


#### Define the Device Type

First, define the device type and how to read the service and characteristic UUID. In the provided code, it is possible to read the LYWSD03MMC. 

The `Data_LYWSD03MMC.__init__` function does not require the Service and Characteristic UUID. They don't change from a LYWSD03MMC to another. The function `Data_LYWSD03MMC.parseNotification(self, obj, value)` define how to read the data. Today the code allows only to read one Service and Characteristic per device. This can be adapted in future versions.

It is important to notice that data are stored in an InfluxDB instance.

## Data storage

It is necessary to install an InxludDB. To use another database, the code needs to be adapted. (It is possible but not planned.)
The `00_install.sh` script installs the InfluxDB on the raspberry.

```sh
# InfluxDB
sudo apt install influxdb
pip install influxdb

sudo systemctl start influxdb
sudo systemctl enable influxdb
```

The credential of the database is stored in the `lib/IndeluxDB.py` file. In future version, credentials will be stored in other files.

## Grafana

The `00_install.sh` script installs Grafana on the raspberry.

On the installation is done, you can create the panel and dashboard as you want. To access it, go to `http://<ip_raspberry>:3000`.