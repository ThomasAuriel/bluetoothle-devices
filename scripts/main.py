import threading
from bluepy.btle import Peripheral, DefaultDelegate, BTLEGattError
from datetime import datetime
from lib import InfluxDB


class Data:
    def __init__(
        self, serviceUUID: int, charUUID: int, descUUID: int, localisation: str, sensorType: str
    ):
        """
        Initialize a Data object with serviceUUID, charUUID, and descUUID.

        Parameters
        ----------
        serviceUUID: int
            The service UUID.
        charUUID: int
            The characteristic UUID.
        descUUID: int
            The descriptor UUID.
        localisation: str
            Location where the data is collected.
        sensorType: str
            Type of sensor collecting the data.

        Return
        ------
        None

        Examples
        --------
        data = Data(0x180F, 0x2A19, 0x2902, "Desktop", "Charge")

        Notes
        -----
        """
        self.serviceUUID, self.charUUID, self.descUUID = serviceUUID, charUUID, descUUID
        self.localisation, self.sensorType = localisation, sensorType
        self.cHandle = None

        self.localisation = localisation
        self.sensorType = sensorType

    def handleData(self, data):
        """
        Handle incoming data and print it.

        Parameters
        ----------
        data: bytes
            The incoming raw data.

        Return
        ------
        None

        Examples
        --------
        handleData(b'\x01\x02\x03')

        Notes
        -----
        - This is a base method and should be overridden in subclasses.
        """
        print(f"Data from {self.serviceUUID}/{self.charUUID}: {data}")

    def log_to_influx(self, measurement, field, value, unit):
        """
        Log data to InfluxDB.

        Parameters
        ----------
        measurement: str
            The type of measurement (e.g., 'temperature', 'humidity').
        field: str
            The field name for the data point (e.g., 'temperature', 'humidity').
        value: float
            The value of the data point.
        unit: str
            The unit of the data point (e.g., '°C', '%').

        Return
        ------
        None

        Examples
        --------
        log_to_influx('air temperature', 'temperature', 22.5, '°C')

        Notes
        -----
        - This method will connect to InfluxDB and log the data point.
        """
        time = datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")
        print(f"{time} - {self.localisation} : {field} = {value:.1f} {unit}")

        data_point = {
            "measurement": measurement,
            "tags": {
                "localisation": self.localisation,
                "sensorType": self.sensorType,
                "unit": unit,
            },
            "time": time,
            "fields": {field: value},
        }

        InfluxDB.connect_to_influxdb()
        InfluxDB.create_database(client=InfluxDB.client, database_name="AurielHouse")
        InfluxDB.add_data(
            client=InfluxDB.client, database_name="AurielHouse", data_points=[data_point]
        )


class Data_ATC_Temp(Data):
    def handleData(self, data):
        temp = int.from_bytes(data, "little", signed=True) / 100
        self.log_to_influx("air temperature", "temperature", temp, "°C")


class Data_ATC_Humi(Data):
    def handleData(self, data):
        humi = int.from_bytes(data, "little") / 100
        self.log_to_influx("air relative humidity", "humidity", humi, "%")


class Data_ATC_Volt(Data):
    def handleData(self, data):
        volt = int.from_bytes(data, "little")
        self.log_to_influx("battery level", "charge", volt, "%")


class Sensor(DefaultDelegate):
    def __init__(self, address, iface, data_objects):
        self.address, self.iface, self.data_objects = address, iface, data_objects
        self.peripheral, self.handle_to_data = None, {}

    def connect(self, nbAttempt=5):
        """
        Attempt to connect to the peripheral device.

        Parameters
        ----------
        nbAttempt: int
            Number of connection attempts. Default is 5. Use -1 for infinite attempts.

        Return
        ------
        None

        Examples
        --------
        connect(3)

        Notes
        -----
        - This method will print status messages during the connection attempts.
        """
        attempt = 0
        while self.peripheral is None:
            try:
                self.peripheral = Peripheral(self.address, self.iface)
                print(f"{self.address} : Connected")
            except:
                if nbAttempt >= 0:
                    if attempt >= nbAttempt:
                        print("Max attempts reached. Fail to connect")
                        return
                attempt += 1
                print(f"{self.address} : Attempt {attempt} failed. Retry")

    def disconnect(self):
        """
        Disconnect from the peripheral device.

        Parameters
        ----------
        None

        Return
        ------
        None

        Examples
        --------
        disconnect()

        Notes
        -----
        - This method will set the 'peripheral' attribute to None.
        """
        if self.peripheral is not None:
            self.peripheral.disconnect()
            self.peripheral = None

    def get_all_services_and_characteristics(self):
        """
        Retrieve and print all services and characteristics from the peripheral device.

        Parameters
        ----------
        None

        Return
        ------
        None

        Examples
        --------
        get_all_services_and_characteristics()

        Notes
        -----
        - This method will print all services, characteristics, and descriptors.
        """
        self.connect()

        for service in self.peripheral.getServices():
            try:
                print(f"Service: {service} {service.uuid}")
                for char in service.getCharacteristics():
                    print(f"  Characteristic: {char} - {char.uuid}")
                    for desc in char.getDescriptors():
                        print(f"    Descriptor: {desc} - {desc.uuid}")
            except BTLEGattError as e:
                print(e)
            finally:
                pass

    def listen_to_device(self):
        """
        Listen for notifications from the peripheral device.

        Parameters
        ----------
        None

        Return
        ------
        None

        Examples
        --------
        listen_to_device()

        Notes
        -----
        - This method will run indefinitely, listening for notifications.
        """
        self.connect(nbAttempt=-1)  # Infinite retries

        for data_obj in self.data_objects:
            svc = self.peripheral.getServiceByUUID(data_obj.serviceUUID)
            char = svc.getCharacteristics(data_obj.charUUID)[0]
            data_obj.cHandle = char.getHandle()
            self.handle_to_data[char.getHandle()] = data_obj
            char.getDescriptors(forUUID=data_obj.descUUID)[0].write(b"\x01\x00", True)

        self.peripheral.setDelegate(self)

        while True:
            if self.peripheral.waitForNotifications(1.0):
                continue

    def handleNotification(self, cHandle, data):
        """
        Handle incoming notifications.

        Parameters
        ----------
        cHandle: int
            The characteristic handle.
        data: bytes
            The incoming raw data.

        Return
        ------
        None

        Examples
        --------
        handleNotification(42, b'\x01\x02\x03')

        Notes
        -----
        - This method will call 'handleData' on the appropriate Data object.
        """
        data_obj = self.handle_to_data.get(cHandle)
        if data_obj:
            data_obj.handleData(data)


# Initialize Sensor objects
sensors = [
    Sensor(
        "A4:C1:38:1F:53:78",
        "public",
        [
            Data_ATC_Volt(0x180F, 0x2A19, 0x2902, "Desktop", "Charge"),
            Data_ATC_Temp(0x181A, 0x2A6E, 0x2902, "Desktop", "Temp"),
            Data_ATC_Humi(0x181A, 0x2A6F, 0x2902, "Desktop", "Humi"),
        ],
    ),
    Sensor(
        "A4:C1:38:1B:3F:93",
        "public",
        [
            Data_ATC_Volt(0x180F, 0x2A19, 0x2902, "Bathroom", "Charge"),
            Data_ATC_Temp(0x181A, 0x2A6E, 0x2902, "Bathroom", "Temp"),
            Data_ATC_Humi(0x181A, 0x2A6F, 0x2902, "Bathroom", "Humi"),
        ],
    ),
    Sensor(
        "A4:C1:38:A9:FE:FF",
        "public",
        [
            Data_ATC_Volt(0x180F, 0x2A19, 0x2902, "Winter garden", "Charge"),
            Data_ATC_Temp(0x181A, 0x2A6E, 0x2902, "Winter garden", "Temp"),
            Data_ATC_Humi(0x181A, 0x2A6F, 0x2902, "Winter garden", "Humi"),
        ],
    ),
    Sensor(
        "A4:C1:38:C1:63:BA",
        "public",
        [
            Data_ATC_Volt(0x180F, 0x2A19, 0x2902, "Outside", "Charge"),
            Data_ATC_Temp(0x181A, 0x2A6E, 0x2902, "Outside", "Temp"),
            Data_ATC_Humi(0x181A, 0x2A6F, 0x2902, "Outside", "Humi"),
        ],
    )
]

# Create threads for each sensor
threads = [threading.Thread(target=sensor.listen_to_device) for sensor in sensors]
# Start and wait for all threads to complete
[t.start() for t in threads]
[t.join() for t in threads]
