from influxdb import InfluxDBClient
from typing import List, Dict, Any

# Define InfluxDB connection parameters
host = "localhost"
port = 8086
username = "your_username"
password = "your_password"

client = None


def connect_to_influxdb():
    """
    Connect to InfluxDB and return the client object.

    Parameters
    --------------
    None

    Return
    --------
    InfluxDBClient
        The InfluxDB client object.

    Example
    -----
    influx_client = connect_to_influxdb()
    """

    global client
    if client is None:
        client = InfluxDBClient(host=host, port=port)#, username=username, password=password)
    return client


def create_database(client, database_name):
    """
    Create a new database in InfluxDB.

    Parameters
    --------------
    client: InfluxDBClient
        The InfluxDB client object.
    database_name: str
        The name of the database to create.

    Return
    --------
    None

    Example
    -----
    create_database(influx_client, 'mydatabase')
    """

    client.create_database(database_name)


def delete_measurement(client, database_name):
    """
    Delete a measurement (table) from an InfluxDB database.

    Parameters
    --------------
    client: InfluxDBClient
        The InfluxDB client object.
    measurement_name: str
        The name of the measurement (table) to delete.

    Return
    --------
    None

    Example
    -----
    delete_measurement(client, 'my_measurement')
    """

    client.switch_database(database_name)
    client.query(f"DROP MEASUREMENT {database_name}")


def add_data(client, database_name, data_points: List[Dict[str, Any]]):
    """
    Add data points to an InfluxDB database.

    Parameters
    --------------
    client: InfluxDBClient
        The InfluxDB client object.
    database_name: str
        The name of the database.
    data_points: List[Dict[str, Any]]
        A list of dictionaries, each containing a data point.

    Return
    --------
    None

    Example
    -----
    data_points = [
        {
            'measurement': 'my_measurement',
            'tags': {'tag1': 'value1'},
            'time': '2023-09-10T12:00:00Z',
            'fields': {'field1': 42, 'field2': 3.14}
        },
        # Add more data points as needed
    ]
    add_data(influx_client, 'mydatabase', data_points)
    """

    client.switch_database(database_name)
    client.write_points(points=data_points)


def remove_data(client, database_name, measurement, where_clause):
    """
    Remove data points from an InfluxDB measurement based on a WHERE clause.

    Parameters
    --------------
    client: InfluxDBClient
        The InfluxDB client object.
    database_name: str
        The name of the database.
    measurement: str
        The measurement name to remove data from.
    where_clause: str
        A WHERE clause to specify which data points to remove.

    Return
    --------
    None

    Example
    -----
    remove_data(influx_client, 'mydatabase', 'my_measurement', "time > '2023-09-10T12:00:00Z'")
    """

    client.switch_database(database_name)
    query = f"DELETE FROM {measurement} WHERE {where_clause}"
    client.query(query)


# Example usage:
if __name__ == "__main__":
    # Connect to InfluxDB
    influx_client = connect_to_influxdb()

    # Create a new database
    create_database(influx_client, "mydatabase")

    # Add data to the database
    data_points = [
        {
            "measurement": "my_measurement",
            "tags": {"tag1": "value1"},
            "time": "2023-09-10T12:00:00Z",
            "fields": {"field1": 42, "field2": 3.14},
        },
        # Add more data points as needed
    ]
    add_data(influx_client, "mydatabase", "my_measurement", data_points)

    # Remove data based on a WHERE clause
    remove_data(influx_client, "mydatabase", "my_measurement", "time > '2023-09-10T12:00:00Z'")

    # Close the InfluxDB client connection
    influx_client.close()
